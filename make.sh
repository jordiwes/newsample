#!/bin/sh
# -------
#
# > ./make.sh library
#
# examples:
#   full build xmlservice library (product xml)
#   > ./make.sh ZENDSVR6
#
#   individual module compiles may compile all RPGMAINs (product zs6)
#   > ./make.sh ZENDSVR6 ez1pgm ez1srv
#
# -------
RPGLIB=''
RPGFILES='ez1srv ez1pgm'
MYDIR=$(pwd)

# -------
# user input
# -------
if [ "$#" -gt 0 ]
then
  RPGLIB=$1
fi

# help
if [[ -z "$RPGLIB" ]]
then
  echo "Usage: $0 library"
  exit
fi

# user specific modules
if [ "$#" -gt 1 ]
then
  RPGFILES="$2 $3 $4 $5 $6 $7 $8 $9"
fi

# -------
# build core modules
# -------
for i in $RPGFILES; do
  echo '===================================='
  echo "==> $RPGLIB/$i ..."
  system "DLTMOD MODULE($RPGLIB/$i)"
  system "CRTRPGMOD MODULE($RPGLIB/$i) SRCSTMF('$i.rpgle') DBGVIEW(*SOURCE) OUTPUT(*PRINT) REPLACE(*YES)" > /dev/null
  if [[ -e "/qsys.lib/$RPGLIB.lib/$i.module" ]]
  then
    echo "==> $RPGLIB/$i.module -- ok"
  else
    system "CRTRPGMOD MODULE($RPGLIB/$i) SRCSTMF('$i.rpgle') DBGVIEW(*SOURCE) OUTPUT(*PRINT) REPLACE(*YES)"
    echo "==> $RPGLIB/$i.module -- failed"
    exit
  fi
  echo '===================================='
done

# -------
# build mains
# -------
for i in $RPGFILES; do
  echo '===================================='
  echo "==> $RPGLIB/$i ..."
  case "$i" in
          "ez1srv")
    cmd="CRTSRVPGM SRVPGM($RPGLIB/$i) MODULE($RPGLIB/$i) EXPORT(*ALL) ACTGRP(*CALLER)"
    system "DLTSRVPGM SRVPGM($RPGLIB/$i)"
    echo "$cmd"
    system "$cmd"
    if [[ -e "/qsys.lib/$RPGLIB.lib/$i.SRVPGM" ]]
    then
      echo "==> $RPGLIB/$i -- ok"
    else
      echo "==> $RPGLIB/$i -- failed"
      exit
    fi
  ;;
	"ez1pgm")
    bndsrv="BNDSRVPGM($RPGLIB/ez1srv)"
    cmd="CRTPGM PGM($RPGLIB/$i) MODULE($RPGLIB/$i) $bndsrv"
    system "DLTPGM PGM($RPGLIB/$i)"
    echo "$cmd"
    system "$cmd"
    if [[ -e "/qsys.lib/$RPGLIB.lib/$i.PGM" ]]
    then
      echo "==> $RPGLIB/$i -- ok"
    else
      echo "==> $RPGLIB/$i -- failed"
      exit
    fi
  ;;
  esac
done

